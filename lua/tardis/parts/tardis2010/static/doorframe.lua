local PART = {}

PART.ID = "tardis2010_doorframe"
PART.Name = "2010 TARDIS Door Frame"
PART.Model = "models/doctorwho1200/copper/doorframe.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)