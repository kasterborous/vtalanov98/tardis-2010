local PART = {}

PART.ID = "tardis2010_furniture"
PART.Name = "2010 TARDIS Furniture"
PART.Model = "models/doctorwho1200/copper/furniture.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)