local PART = {}

PART.ID = "tardis2010_lever2"
PART.Name = "2010 TARDIS Lever 2"
PART.Model = "models/doctorwho1200/copper/lever2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctorwho1200/copper/lever2.wav"

PART.PowerOffSound = true
PART.PowerOffUse = false

TARDIS:AddPart(PART)