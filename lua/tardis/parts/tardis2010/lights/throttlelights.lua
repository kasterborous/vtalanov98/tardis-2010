local PART = {}

PART.ID = "tardis2010_throttlelights"
PART.Name = "2010 TARDIS Throttle Lights"
PART.Model = "models/doctorwho1200/copper/throttlelights.mdl"
PART.AutoSetup = true

TARDIS:AddPart(PART)