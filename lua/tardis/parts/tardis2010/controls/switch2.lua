local PART = {}

PART.ID = "tardis2010_switch2"
PART.Name = "2010 TARDIS Switch 2"
PART.Model = "models/doctorwho1200/copper/switch2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/copper/switch2.wav"

TARDIS:AddPart(PART)