local PART = {}

PART.ID = "tardis2010_lever1"
PART.Name = "2010 TARDIS Lever 1"
PART.Model = "models/doctorwho1200/copper/lever.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 3.5
PART.Sound = "doctorwho1200/copper/leveroff.wav"

TARDIS:AddPart(PART)