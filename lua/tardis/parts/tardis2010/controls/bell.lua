local PART = {}

PART.ID = "tardis2010_bell"
PART.Name = "2010 TARDIS Bell"
PART.Model = "models/doctorwho1200/copper/bell.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Sound = "vtalanov98/tardis2010/bell.wav"

TARDIS:AddPart(PART)