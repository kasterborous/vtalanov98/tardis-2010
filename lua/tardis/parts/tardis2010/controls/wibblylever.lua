local PART = {}

PART.ID = "tardis2010_wibblylever"
PART.Name = "2010 TARDIS Wibbly Lever"
PART.Model = "models/doctorwho1200/copper/wibblylever.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.85
PART.Sound = "vtalanov98/tardis2010/wibblylever.wav"

TARDIS:AddPart(PART)