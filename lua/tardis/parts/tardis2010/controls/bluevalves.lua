local PART = {}
PART.ID = "tardis2010_bluevalves"
PART.Name = "2010 TARDIS Blue Valves"
PART.Model = "models/doctorwho1200/copper/bluevalves.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.8
PART.Sound = "doctorwho1200/copper/bluevalvesoff.wav"

TARDIS:AddPart(PART)