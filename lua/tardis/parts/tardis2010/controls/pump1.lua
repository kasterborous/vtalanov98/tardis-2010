local PART = {}

PART.ID = "tardis2010_pump1"
PART.Name = "2010 TARDIS Pump 1"
PART.Model = "models/doctorwho1200/copper/pump.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.65
PART.Sound = "doctorwho1200/copper/pump.wav"

TARDIS:AddPart(PART)