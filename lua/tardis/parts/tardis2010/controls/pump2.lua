local PART = {}

PART.ID = "tardis2010_pump2"
PART.Name = "2010 TARDIS Pump 2"
PART.Model = "models/doctorwho1200/copper/pump2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.8
PART.Sound = "doctorwho1200/copper/pump2.wav"

TARDIS:AddPart(PART)