local PART = {}

PART.ID = "tardis2010_crank1"
PART.Name = "2010 TARDIS Crank 1"
PART.Model = "models/doctorwho1200/copper/crank.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctorwho1200/copper/crank.wav"

TARDIS:AddPart(PART)