local PART = {}

PART.ID = "tardis2010_corridordoors15_static"
PART.Name = "2010 TARDIS Corridor Doors 15"
PART.Model = "models/doctorwho1200/copper/intdoors15.mdl"
PART.AutoSetup = true
PART.BypassIsomorphic = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = false
PART.Use = false

TARDIS:AddPart(PART)