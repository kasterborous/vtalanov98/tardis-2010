local PART = {}

PART.ID = "tardis2010_corridors3"
PART.Name = "2010 TARDIS Corridors 3"
PART.Model = "models/doctorwho1200/copper/corridors3.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)